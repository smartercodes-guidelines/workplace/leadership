# Leadership

At Smarter.Codes, Leadership is not expected just 'a selected few'. It is rather expected from (almost) EVERYONE in Smarter.Codes. Playing leader is a 'job requirement', not a 'good to have'

Inspired from [Lost and Founder](https://www.goodreads.com/book/show/35957156-lost-and-founder) we would define Leadership as follows
> The best way to ensure that your CTO is going to make you a better CEO is to hire a CTO who likes to teach. Make it clear that you're looking for someone to drive change and educate you and the team. Beware CTOs who try to '"shield" you from the details. Being able to explain complex things simply is a job requirement. Untimately, leading technical teams comes down to curiosity and courage. You must be humble, ask questions, and read a lot about engineering. If incremental changes aren't getting better results, you need the courage to change the technical leadership. Keep changing and trying new things until you start seeing positive momentum, and then you get ouf of th team's way
>
> Sarah, Bird, CEO of Moz

Like Sarah, our definiton of Leadernship if 'someone who can teach'. The above quote was written for 'engineering'. We however apply it to all departments, whether you are contributing in Marketing, or Sales, or Product Design.

Our definition of Leadership is that you should be able to 'teach'. No matter whether you joined Smarter.Codes as an intern. Or as seasoned professional. And as outlined in [point 4 in guidelines](https://gitlab.com/smarter-codes/guidelines/about-guidelines), we all will strive will keep calm and improve - especially if teachings come in from someone who is not having "enough experience" in our industry, or in our team'. We all strive to be lifelong learners

# How good as a Teacher are you ?
During [Performance Appraisal](https://gitlab.com/smarter-codes/guidelines/workplace/performance-appraisal) we would evaluate how well you are as a guru by checking if you are [A. Telling others, B. Teaching others, or C. Involving others](https://www.brainyquote.com/quotes/benjamin_franklin_383997)

Also we would evaluate how many [students surpass you - the guru)(https://www.quora.com/Is-there-any-proverb-or-old-saying-to-use-when-a-student-surpasses-a-teacher). 

# Why Leadership, and Why Teach ?
## Once you become a Leader..
Smarter.Codes is not a Startup. It is a [Startup Studio](https://en.wikipedia.org/wiki/Startup_studio). Which means we are going to have 100s of startups by 2025 (at least that is the aim). By July 2023 we've already 5+ startups with their team fully formed. We need everyone to be a Leader (and hence teacher / guru) so that you are promoted to become Leader to these startups that are spinning out of the startup studio. Each one of us must be a candidate to become CXO of one of these startups, and capable enough to be listed as co-founder and equity owner of the startup.

## You Lead, once you teach selflessly
If you teach your craft to someone else, and that [student becomes better than you]((https://www.quora.com/Is-there-any-proverb-or-old-saying-to-use-when-a-student-surpasses-a-teacher)), only then you can "stop doing your role" and promote yourself to become a [leader doing generalist role](https://www.google.com/search?q=generalist+vs+specialist+leader) 

## If I don't lead..
It is likely that within 6 or maximum 18 months of your joining Smarter.Codes, you would either be successfully accepted as Leader / Interim CXO to one of these startups, or you are seen as a strong candidate for it. If not a CXO you are leading a team of specialists under you. At Smarter.Codes we would not be able to continue to work with individual contributors couldnt become Leaders to one of these startups even after 18 months of their joining. Individual Contributors are amazing Specialists, but not suited for the SmarterCodes culture where we are trying to build Startup Studio than a single startup. And hence we must let these individual contributors join other startups outisde Smarter.Codes where they can shine better in their career.